package me.relevante.searchService.health;

import lombok.val;
import me.relevante.searchService.client.ElasticClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class ElasticHealthIndicator implements HealthIndicator {

    private ElasticClient client;

    @Autowired
    private ElasticHealthIndicator(ElasticClient client) {
        this.client = client;
    }

    @Override
    public Health health() {
        val resp = client.clusterHealth();
        switch (resp.getStatus()) {
            case GREEN:
            case YELLOW:
                return Health.up().build();
            default:
                return Health.down().withDetail("clusterStatus", resp.getStatus()).build();
        }
    }
}
