package me.relevante.searchService.service;

import lombok.val;
import me.relevante.searchService.api.PageReq;
import me.relevante.searchService.client.ElasticResp;
import me.relevante.searchService.client.ElasticClient;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Component
public class SearchService {

    private ElasticClient client;

    @Autowired
    private SearchService(ElasticClient client) {
        this.client = client;
    }

    public ElasticResp searchForIds(String location, String company, String keyWords, PageReq page) throws ExecutionException, InterruptedException {
        val resp = client.prepareSearch("relevante")
                .setQuery(buildQuery(location, company, keyWords))
                .storedFields("id")
                .setSize(page.getSize())
                .setFrom(page.offset())
                .execute().get();

        val ids = Arrays.stream(resp.getHits().hits())
                .map(SearchHit::getId)
                .collect(Collectors.toList());

        return ElasticResp.builder().ids(ids).total(resp.getHits().totalHits()).build();
    }

    private QueryBuilder buildQuery(String location, String company, String keyWords) {

        return QueryBuilders.boolQuery()
                .must(QueryBuilders.termQuery("location", location))
                .must(QueryBuilders.termQuery("company", company))
                .must(QueryBuilders.matchQuery("about", keyWords));
    }
}
