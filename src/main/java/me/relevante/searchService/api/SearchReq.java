package me.relevante.searchService.api;

import lombok.Value;
import org.hibernate.validator.constraints.NotEmpty;

@Value
public class SearchReq {
    @NotEmpty
    String location;
    @NotEmpty
    String company;
    @NotEmpty
    String keyWords;
}
