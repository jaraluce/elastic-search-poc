package me.relevante.searchService.api;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Builder
@Value
public class SearchResp {
    List<String> ids;
    Long total;
}
