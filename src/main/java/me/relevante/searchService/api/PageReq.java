package me.relevante.searchService.api;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class PageReq {
    Integer page;
    Integer size;

    public Integer offset() {
        return page * size;
    }
}
