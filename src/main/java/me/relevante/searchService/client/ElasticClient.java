package me.relevante.searchService.client;

import lombok.val;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.jooq.lambda.Unchecked;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.net.InetAddress;
import java.util.List;

@Component
public class ElasticClient {

    @Value("#{'${elasticsearch.cluster.nodes}'.split(',')}")
    private List<String> clusterNodes;

    @Value("${elasticsearch.cluster.name}")
    private String clusterName;

    private TransportClient client;

    public SearchRequestBuilder prepareSearch(String index) {
        return this.client.prepareSearch(index);
    }

    public ClusterHealthResponse clusterHealth() {
        return client.admin().cluster().prepareHealth().get();
    }

    @PostConstruct
    private void createClient() {
        val transportAddressCreator = Unchecked.function((String[] p) -> new InetSocketTransportAddress(InetAddress.getByName(p[0]), Integer.parseInt(p[1])));

        val transportAddresses = clusterNodes.stream().map(l -> l.split(":")).map(transportAddressCreator).toArray(TransportAddress[]::new);

        val settings = Settings.builder().put("cluster.name", clusterName).build();

        this.client = new PreBuiltTransportClient(settings).addTransportAddresses(transportAddresses);
    }

    @PreDestroy
    private void closeClient() {
        this.client.close();
    }

}
