package me.relevante.searchService.client;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class ElasticResp {
    Long total;
    List<String> ids;
}
