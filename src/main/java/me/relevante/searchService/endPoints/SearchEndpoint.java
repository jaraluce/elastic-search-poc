package me.relevante.searchService.endPoints;

import lombok.val;
import me.relevante.searchService.api.PageReq;
import me.relevante.searchService.api.SearchReq;
import me.relevante.searchService.api.SearchResp;
import me.relevante.searchService.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.concurrent.ExecutionException;

@RestController
class SearchEndpoint {

    private SearchService searchService;

    @Autowired
    private SearchEndpoint(SearchService searchService) {
        this.searchService = searchService;
    }

    @RequestMapping(path = "/search", method = RequestMethod.POST)
    public SearchResp search(
            @Valid @RequestBody SearchReq req,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size) throws ExecutionException, InterruptedException {
        val resp = searchService.searchForIds(req.getLocation(), req.getCompany(), req.getKeyWords(), PageReq.builder().page(page).size(size).build());
        return SearchResp.builder().ids(resp.getIds()).total(resp.getTotal()).build();
    }

}