package me.relevante.searchService.endPoints;

import me.relevante.searchService.api.PageReq;
import me.relevante.searchService.client.ElasticResp;
import me.relevante.searchService.service.SearchService;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(SearchEndpoint.class)
public class SearchEndpointTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private SearchService searchService;

    @Test
    public void shouldReturnTheListOfIds() throws Exception {
        given(this.searchService.searchForIds("Thynedale", "Inrt", "ipsum", PageReq.builder().page(0).size(10).build())).willReturn(ElasticResp.builder().total(2L).ids(Lists.newArrayList("asd", "asd")).build());
        this.mvc
                .perform(post("/search").content("{\"location\": \"Thynedale\", \"company\": \"Inrt\", \"keyWords\": \"ipsum\"}").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.total", is(2)))
                .andExpect(jsonPath("$.ids", hasSize(2)))
                .andExpect(jsonPath("$.ids", contains("asd", "asd")));

    }

    @Test
    public void shouldReturnBadRequestWhenNoBody() throws Exception {
        this.mvc.perform(post("/search"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnBadRequestWhenMissingAnyRequiredParameter() throws Exception {
        this.mvc
                .perform(post("/search").content("{\"company\": \"Inrt\", \"keyWords\": \"ipsum\"}").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());

        this.mvc
                .perform(post("/search").content("{\"location\": \"Thynedale\", \"keyWords\": \"ipsum\"}").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());

        this.mvc
                .perform(post("/search").content("{\"location\": \"Thynedale\", \"company\": \"Inrt\"}").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnUnsupportedMediaTypeWhenNoContentTypeSpecified() throws Exception {
        this.mvc.perform(post("/search").content("{}"))
                .andExpect(status().isUnsupportedMediaType());
    }
}
